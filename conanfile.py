from conans import ConanFile, tools


class Utf8cppConan(ConanFile):
    name = "UTF8-CPP"
    version = "2.3.5"
    license = "BSL-1.0"
    url = "https://gitlab.com/monlib/utf8-cpp-conan/"
    description = "UTF-8 with C++ in a Portable Way"
    no_copy_source = True
    # No settings/options are necessary, this is header only

    def source(self):
        tools.download("https://github.com/nemtrif/utfcpp/archive/v2.3.5.zip", "f.zip")
        tools.unzip("f.zip")

    def package(self):
        self.copy("*.h", "include", "utfcpp-2.3.5/source")
